use std::env;
use std::fmt;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;
use std::time::{Instant, Duration};

extern crate failure;
extern crate futures;
use futures::{Future, Stream, future::Executor};

extern crate rand;
use rand::Rng;

extern crate tokio_core;
use tokio_core::reactor::{Core, Interval};

extern crate buffered_reader;
use buffered_reader::BufferedReader;
extern crate sequoia_openpgp as openpgp;
extern crate sequoia_core as core;
extern crate sequoia_net as net;

struct Hat<'a> {
    fp: buffered_reader::File<'a, ()>,
}

impl<'a> Hat<'a> {
    fn new(p: &str) -> openpgp::Result<Self> {
        let mut fp = buffered_reader::File::open(p)?;
        if fp.data_eof()?.len() % 20 != 0 {
            return Err(failure::format_err!(
                "Bad fingerprint dump, len is {} mod 20",
                fp.data_eof()?.len() % 20));
        }
        Ok(Hat { fp })
    }

    fn draw(&mut self) -> openpgp::Fingerprint {
        let fp_raw = self.fp.data_eof().unwrap();
        let fp_len = fp_raw.len() / 20;
        let i = rand::thread_rng().gen_range(0, fp_len);
        let mut raw = fp_raw[i * 20..(i + 1) * 20].to_vec();
        if false && rand::random::<f32>() > 0.9 {
            raw.iter_mut().for_each(|b| *b = ! *b);
        }
        openpgp::Fingerprint::from_bytes(&raw)
    }
}

struct Counter {
    start: Instant,
    requests: AtomicUsize,
    responses: AtomicUsize,
}

impl Counter {
    fn new() -> Self {
        Counter {
            start: Instant::now(),
            requests: AtomicUsize::new(0),
            responses: AtomicUsize::new(0),
        }
    }

    fn in_flight(&self) -> usize {
        let requests = self.requests.load(Ordering::Relaxed);
        let responses = self.responses.load(Ordering::Relaxed);
        requests - responses
    }
}

impl fmt::Display for Counter {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let elapsed = self.start.elapsed();
        let responses = self.responses.load(Ordering::Relaxed);
        write!(f, "{} keys in {:?}, {}/s", responses, elapsed,
               (responses as f32 / (elapsed.as_secs() as f32
                 + elapsed.subsec_millis() as f32 / 1000.)))
    }
}

fn real_main() -> openpgp::Result<()> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 && args.len() != 4 {
        return Err(failure::format_err!(
            "HKP load test.\n\n\
             Usage: {} <fingerprint-dump> <HKP-URL> [<parallel-requests>]\n",
            args[0]));
    }

    let mut hat = Hat::new(&args[1])?;
    let counter = Arc::new(Counter::new());

    let ctx = core::Context::new()?;
    let mut ks = net::async::KeyServer::new(&ctx, &args[2])?;
    let mut core = Core::new()?;

    let n: usize = args.get(3).map(|s| s.as_str()).unwrap_or_else(|| "100")
        .parse().expect("invalid number");

    let counter_status = counter.clone();
    let counter_interval = counter.clone();
    let handle = core.handle();
    core.execute(
        Interval::new(Duration::from_millis(50), &core.handle())?
        .for_each(move |_| {
            let in_flight = counter_interval.in_flight();
            for _ in 0..n - in_flight {
                counter.requests.fetch_add(1, Ordering::Relaxed);
                let counter_ok = counter.clone();
                let counter_err = counter.clone();
                let _ = handle.execute(
                    ks.get(&hat.draw().to_keyid())
                        .and_then(move |_| {
                            counter_ok.responses.fetch_add(1, Ordering::Relaxed);
                            eprint!("."); Ok(()) })
                        .map_err(move |e| {
                            counter_err.responses.fetch_add(1, Ordering::Relaxed);
                            if let Some(e) = e.downcast_ref::<net::Error>() {
                                return match e {
                                    net::Error::NotFound =>
                                        eprint!("4"),
                                    net::Error::MalformedResponse =>
                                        eprint!("G"),
                                    net::Error::ProtocolViolation =>
                                        eprint!("P"),
                                    _ =>
                                        eprintln!("KeyServer::get() failed: {}",
                                                  e),
                                }
                            } else {
                                eprintln!("KeyServer::get() failed: {}", e);
                            }
                        }))
                    .map_err(|e| eprintln!("Failed to spawn task: {:?}",
                                           e.kind()));
            }
            Ok(())
        })
            .map_err(move |e| {
                eprintln!("Scheduler: {}", e);
                ()
            }))
        .map_err(|e| failure::format_err!("Failed to spawn task: {:?}",
                                          e.kind()))?;

    let status =
        Interval::new(Duration::new(2, 0), &core.handle())?
        .for_each(move |_| {
            Ok(eprintln!("\n{}\n", counter_status))
        });
    //let timeout = Timeout::new(Duration::new(300, 0), &core.handle())?;
    core.run(status)?;
    Ok(())
}

fn main() {
    if let Err(e) = real_main() {
        let mut cause = e.as_fail();
        eprint!("{}", cause);
        while let Some(c) = cause.cause() {
            eprint!(":\n  {}", c);
            cause = c;
        }
        eprintln!();
        std::process::exit(2);
    }
}
